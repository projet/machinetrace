
/* MachineTrace - Encore une nouvelle version (à but pédagogique) de la célèbre tortue LOGO
 * Copyright (C) 2018 Guillaume Huard
 * 
 * Ce programme est libre, vous pouvez le redistribuer et/ou le
 * modifier selon les termes de la Licence Publique Générale GNU publiée par la
 * Free Software Foundation (version 2 ou bien toute autre version ultérieure
 * choisie par vous).
 * 
 * Ce programme est distribué car potentiellement utile, mais SANS
 * AUCUNE GARANTIE, ni explicite ni implicite, y compris les garanties de
 * commercialisation ou d'adaptation dans un but spécifique. Reportez-vous à la
 * Licence Publique Générale GNU pour plus de détails.
 * 
 * Vous devez avoir reçu une copie de la Licence Publique Générale
 * GNU en même temps que ce programme ; si ce n'est pas le cas, écrivez à la Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307,
 * États-Unis.
 * 
 * Contact:
 *          Guillaume.Huard@imag.fr
 *          Laboratoire LIG
 *          700 avenue centrale
 *          Domaine universitaire
 *          38401 Saint Martin d'Hères
 */

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;

import javax.imageio.ImageIO;

/**
 * Cette classe hérite de MachineTrace, elle l'étend en y ajoutant de nouvelles
 * primitives de dessin.
 * 
 * @author Guillaume Huard
 * @version 29/06/2018
 */
public class MachineDessin extends MachineTrace {
	EtatDessin etat;
	HashMap<String, Image> hash;

	class EtatDessin extends Etat {

		EtatDessin(int l, int h) {
			super(l, h);
		}

		synchronized void ovale(double rX, double rY) {
			if (enBas) {
				int x = convertX(position.getX() - rX);
				int y = convertY(position.getY() + rY);
				int dX = convert(2 * rX);
				int dY = convert(2 * rY);
				Oval c = new Oval(x, y, dX, dY, color);
				commands.add(c);
			}
		}

		synchronized void image(Image img, double w, double h) {
			if (enBas) {
				double tX = -w / 2;
				double tY = h / 2;

				if (angle != 0) {
					double rot = Math.atan2(tY, tX);
					double norme = Math.sqrt(tX * tX + tY * tY);
					rot += angle;
					tX = norme * Math.cos(rot);
					tY = norme * Math.sin(rot);
				}
				int x = convertX((position.getX() + tX));
				int y = convertY((position.getY() + tY));
				Image i = img.getScaledInstance((int) w, (int) h, Image.SCALE_DEFAULT);
				AffineTransform tr = new AffineTransform();
				tr.setToTranslation(x, y);
				if (angle != 0)
					tr.rotate(-angle);
				Dessin d = new Dessin(i, tr);
				commands.add(d);
			}
		}
	}

	class Oval extends Command {

		int x, y, dX, dY;
		Color col;

		Oval(int a, int b, int c, int d, Color co) {
			x = a;
			y = b;
			dX = c;
			dY = d;
			col = co;
		}

		@Override
		void draw(Graphics2D drawable, int width, int height) {
			drawable.setPaint(col);
			drawable.drawOval(x, y, dX, dY);
		}

	}

	class Dessin extends Command {
		Image img;
		AffineTransform tr;

		Dessin(Image i, AffineTransform t) {
			img = i;
			tr = t;
		}

		@Override
		void draw(Graphics2D drawable, int width, int height) {
			drawable.drawImage(img, tr, null);
		}
	}

	/**
	 * Si la plume est baissée, trace un cercle autour de la position courante de la
	 * plume.
	 * 
	 * @param r
	 *            Rayon du cercle
	 */
	public void cercle(double r) {
		etat.ovale(r, r);
		commit();
	}

	/**
	 * Si la plume est baissée, trace un ovale autour de la position courante de la
	 * plume.
	 * 
	 * @param rX
	 *            Rayon en abscisse de l'ovale
	 * @param rY
	 *            Rayon en ordonnée de l'ovale
	 */
	public void ovale(double rX, double rY) {
		etat.ovale(rX, rY);
		commit();
	}

	/**
	 * Si la plume est baissée, trace une image centrée autour de la position
	 * courante. L'image subit une rotation égale à l'orientation de la plume.
	 * 
	 * @param name
	 *            nom du fichier contenant l'image
	 * @param w
	 *            largeur de l'image à tracer
	 * @param h
	 *            hauteur de l'image à tracer
	 */
	public void image(String name, double w, double h) {
		Image img = null;
		if (hash.containsKey(name)) {
			img = hash.get(name);
		} else {
			InputStream f = null;
			try {
				try {
					f = new FileInputStream(name);
					img = ImageIO.read(f);
					f.close();
				} catch (Exception e) {
					f = ClassLoader.getSystemClassLoader().getResourceAsStream(name);
					if (f == null) {
						System.err.println("Erreur, impossible d'ouvrir " + name);
						return;
					} else {
						img = ImageIO.read(f);
					}
				}
			} catch (Exception e) {
				System.err.println(e);
				return;
			}
			hash.put(name, img);
		}
		etat.image(img, w, h);
		commit();
	}

	/**
	 * Crée une nouvelle fenêtre de dessin.
	 * 
	 * @param l
	 *            Largeur de la fenêtre
	 * @param h
	 *            Hauteur de la fenêtre
	 */
	public MachineDessin(int l, int h) {
		super(l, h);
		etat = (EtatDessin) super.etat;
		hash = new HashMap<String, Image>();
	}

	@Override
	protected Etat creerEtat(int l, int h) {
		return new EtatDessin(l, h);
	}
}