class CerclesAlignes
{
    public static void main(String [] args) {
        MachineDessin m = new MachineDessin(400,400);
        int r=3, n=4;
        
        // Attend automatiquement après chaque commande
        // Permet de voir le dessin se construire
        m.attenteAutomatique(100);
        
        m.baisser();
        m.ovale(150, 75);
     	m.lever();
        m.placer(-100, 0);
        for (int i=0; i<=n; i++) {
        		m.baisser();
        		m.cercle(r);
        		m.lever();
        		m.avancer(4*r);
        		r*=2;
        }
    }
}
