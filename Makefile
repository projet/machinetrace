CC=clang
CFLAGS=-Wall -Werror

PROGRAMS=un_segment segments_alignes cercles_alignes images

all: $(PROGRAMS)

$(PROGRAMS): %: %.o machine_trace.o

un_segment.o: machine_trace.h
segments_alignes.o: machine_trace.h
cercles_alignes.o: machine_trace.h machine_dessin.h

machine_trace.o: machine_trace.h machine_dessin.h MachineTrace.jar

MachineTrace.jar: MachineTrace.java MachineDessin.java Interprete.java
	-rm *.class
	javac $^
	jar cfe $@ Interprete *.class
	rm *.class

clean:
	-rm *.class *.o $(PROGRAMS) MachineTrace.jar
