#include "machine_trace.h"

void tracer_segment_progressivement(machine_trace_t m, double x, double y, double angle, double l) {
    mt_lever(m);
    mt_placer(m, x, y);
    mt_orienter(m, angle);
    mt_baisser(m);
    for (double i = 0; i < l; i += 10) {
        mt_avancer(m, 10);
    }
}

int choisirCouleur(int i) {
    if (i == 0)
        return MT_NOIR;
    else if (i == 1)
        return MT_BLANC;
    else if (i == 2)
        return MT_JAUNE;
    else if (i == 3)
        return MT_ROUGE;
    else if (i == 4)
        return MT_VERT;
    else if (i == 5)
        return MT_BLEU;
    else
        return MT_NOIR;
}

int main() {
    machine_trace_t m;

    m = mt_creer(400, 400);
    int n = 20, d = 10;
    double x, y;

    // Attend automatiquement après chaque commande
    // Permet de voir le dessin se construire
    mt_attente_automatique(m, 10);

    // Première série de tracés sans le pointeur
    mt_masquer_pointeur(m);
    for (int i = 0; i <= n; i++) {
        x = -50;
        y = i * d - 100;
        mt_change_couleur(m, choisirCouleur(i % 6));
        tracer_segment_progressivement(m, x, y, 45, 100);
    }

    // Seconde série de tracés avec le pointeur
    mt_effacer_tout(m);
    mt_montrer_pointeur(m);
    for (int i = 0; i <= n; i++) {
        x = 50;
        y = i * d - 100;
        mt_change_couleur(m, choisirCouleur(i % 6));
        tracer_segment_progressivement(m, x, y, 135, 100);
    }
    mt_fermer(m);

    return 0;
}
