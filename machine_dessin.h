#ifndef __MACHINE_DESSIN_H__
#define __MACHINE_DESSIN_H__
#include "machine_trace.h"

void mt_cercle(machine_trace_t m, double r);
void mt_ovale(machine_trace_t m, double rx, double ry);
void mt_image(machine_trace_t m, char *name, double w, double h);

#endif
