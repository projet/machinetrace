#include "machine_dessin.h"

int main() {
    machine_trace_t m;

    m = mt_creer(400, 400);

    mt_rafraichissement_automatique(m, 0);
    mt_baisser(m);
    for (int i = 0; i <= 360; i++) {
        mt_effacer_tout(m);
        mt_image(m, "curiosity.png", 100, 100);
        mt_tourner_gauche(m, 1);
        mt_rafraichir(m);
        mt_attendre(m, 10);
    }
    mt_fermer(m);
}
