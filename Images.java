class Images {
	public static void main(String[] args) {
		MachineDessin m = new MachineDessin(400, 400);

		m.rafraichissementAutomatique(false);
		m.baisser();
		for (int i = 0; i <= 360; i++) {
			m.effacerTout();
			m.image("curiosity.png", 100, 100);
			m.tournerGauche(1);
			m.rafraichir();
			m.attendre(10);
		}
	}
}
