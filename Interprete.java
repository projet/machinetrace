
/* MachineTrace - Encore une nouvelle version (à but pédagogique) de la célèbre tortue LOGO
 * Copyright (C) 2018 Guillaume Huard
 * 
 * Ce programme est libre, vous pouvez le redistribuer et/ou le
 * modifier selon les termes de la Licence Publique Générale GNU publiée par la
 * Free Software Foundation (version 2 ou bien toute autre version ultérieure
 * choisie par vous).
 * 
 * Ce programme est distribué car potentiellement utile, mais SANS
 * AUCUNE GARANTIE, ni explicite ni implicite, y compris les garanties de
 * commercialisation ou d'adaptation dans un but spécifique. Reportez-vous à la
 * Licence Publique Générale GNU pour plus de détails.
 * 
 * Vous devez avoir reçu une copie de la Licence Publique Générale
 * GNU en même temps que ce programme ; si ce n'est pas le cas, écrivez à la Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307,
 * États-Unis.
 * 
 * Contact:
 *          Guillaume.Huard@imag.fr
 *          Laboratoire LIG
 *          700 avenue centrale
 *          Domaine universitaire
 *          38401 Saint Martin d'Hères
 */

import java.util.NoSuchElementException;
import java.util.Scanner;

public class Interprete {
	static Couple[] commands;
	static int nbCommands;

	interface Action {
		void execute(MachineDessin m, String[] words);
	}

	static class Couple {
		String command;
		Action traitement;

		Couple(String s, Action a) {
			command = s;
			traitement = a;
		}
	}

	static void init() {
		nbCommands = 0;
		commands = new Couple[100];
		commands[nbCommands++] = new Couple("avancer", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.avancer(Double.parseDouble(words[1]));
			}
		});
		commands[nbCommands++] = new Couple("baisser", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.baisser();
			}
		});
		commands[nbCommands++] = new Couple("couleur", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.changeCouleur(Integer.parseInt(words[1]));
			}
		});
		commands[nbCommands++] = new Couple("effacer", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.effacerTout();
			}
		});
		commands[nbCommands++] = new Couple("lever", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.lever();
			}
		});
		commands[nbCommands++] = new Couple("masquer", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.masquerPointeur();
			}
		});
		commands[nbCommands++] = new Couple("montrer", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.montrerPointeur();
			}
		});
		commands[nbCommands++] = new Couple("orienter", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.orienter(Double.parseDouble(words[1]));
			}
		});
		commands[nbCommands++] = new Couple("placer", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.placer(Double.parseDouble(words[1]), Double.parseDouble(words[2]));
			}
		});
		commands[nbCommands++] = new Couple("rafraichir", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.rafraichir();
			}
		});
		commands[nbCommands++] = new Couple("rafraichissement", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.rafraichissementAutomatique(Boolean.parseBoolean(words[1]));
			}
		});
		commands[nbCommands++] = new Couple("tourner", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.tournerGauche(Double.parseDouble(words[1]));
			}
		});
		commands[nbCommands++] = new Couple("cercle", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.cercle(Double.parseDouble(words[1]));
			}
		});
		commands[nbCommands++] = new Couple("image", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.image(words[1], Double.parseDouble(words[2]), Double.parseDouble(words[3]));
			}
		});
		commands[nbCommands++] = new Couple("ovale", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.ovale(Double.parseDouble(words[1]), Double.parseDouble(words[2]));
			}
		});
		commands[nbCommands++] = new Couple("attendre", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.attendre(Integer.parseInt(words[1]));
			}
		});
		commands[nbCommands++] = new Couple("attente", new Action() {
			public void execute(MachineDessin m, String[] words) {
				m.attenteAutomatique(Integer.parseInt(words[1]));
			}
		});
	}

	public static void main(String[] args) {
		int l = Integer.parseInt(args[0]);
		int h = Integer.parseInt(args[1]);
		MachineDessin m = new MachineDessin(l, h);
		Scanner in = new Scanner(System.in);

		init();
		while (true) {
			try {
				String line = in.nextLine();
				String[] words = line.split("\\s+");
				boolean hasPrefix = false;
				Couple command = null;
				
				for (int i=0; i<nbCommands; i++) {
					if (commands[i].command.startsWith(words[0])) {
						if (hasPrefix) {
							if (command == null) {
								System.err.print(" " + commands[i].command);
							} else {
								System.err.print("Commande ambigue, completions possibles : " + command.command + " " + commands[i].command);
								command = null;
							}
						} else {
							hasPrefix = true;
							command = commands[i];
						}
					}
				}
				
				if (hasPrefix) {
					if (command == null)
						System.err.println();
					else
						command.traitement.execute(m, words);
				} else {
					System.err.println("Commande inconnue : " + words[0]);
				}
			} catch (NoSuchElementException e) {
				break;
			}
		}
		m.fermer();
		in.close();
	}
}
