#include "machine_trace.h"

int main() {
    machine_trace_t m;

    m = mt_creer(400, 400);
    mt_placer(m, 0, 0);
    mt_baisser(m);
    mt_placer(m, 100, 100);
    mt_lever(m);
    mt_fermer(m);

    return 0;
}

