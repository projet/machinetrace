#include "machine_trace.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

#define commande(m, ...) (fprintf(m->out, __VA_ARGS__), fprintf(m->out, "\n"), fflush(m->out))

struct machine_trace_data {
    FILE *out;
    pid_t pid;
};

machine_trace_t mt_creer(int l, int h) {
    char largeur[20], hauteur[20];
    snprintf(largeur, 20, "%d", l);
    snprintf(hauteur, 20, "%d", h);
    machine_trace_t m = malloc(sizeof(struct machine_trace_data));
    assert(m != NULL);
    int tube[2];
    assert(pipe(tube) != -1);
    m->pid = fork();
    switch (m->pid) {
        case -1:
            free(m);
            return NULL;
        case 0:
            assert(close(tube[1]) != -1);
            assert(dup2(tube[0], 0) != -1);
            assert(close(tube[0]) != -1);
            assert(execlp("java", "java", "-jar", "MachineTrace.jar", largeur, hauteur, NULL) != -1);
        default:
            assert(close(tube[0]) != -1);
            m->out = fdopen(tube[1], "w");
            assert(m->out != NULL);
    }
    return m;
}

void mt_fermer(machine_trace_t m) {
    assert(waitpid(m->pid, NULL, 0) != -1);
    assert(fclose(m->out) == 0);
    free(m);
}

void mt_attendre(machine_trace_t m, int ms) {
    commande(m, "attendre %d", ms);
}

void mt_attente_automatique(machine_trace_t m, int ms) {
    commande(m, "attente %d", ms);
}

void mt_avancer(machine_trace_t m, double L) {
    commande(m, "avancer %g", L);
}

void mt_baisser(machine_trace_t m) {
    commande(m, "baisser");
}

void mt_change_couleur(machine_trace_t m, int couleur) {
    commande(m, "couleur %d", couleur);
}

void mt_effacer_tout(machine_trace_t m) {
    commande(m, "effacer");
}

void mt_lever(machine_trace_t m) {
    commande(m, "lever");
}

void mt_masquer_pointeur(machine_trace_t m) {
    commande(m, "masquer");
}

void mt_montrer_pointeur(machine_trace_t m) {
    commande(m, "montrer");
}

void mt_orienter(machine_trace_t m, double a) {
    commande(m, "orienter %g", a);
}

void mt_placer(machine_trace_t m, double x, double y) {
    commande(m, "placer %g %g", x, y);
}

void mt_rafraichir(machine_trace_t m) {
    commande(m, "rafraichir");
}

void mt_rafraichissement_automatique(machine_trace_t m, int r) {
    if (r)
        commande(m, "rafraichissement true");
    else
        commande(m, "rafraichissement false");
}

void mt_reculer(machine_trace_t m, double L) {
    commande(m, "avancer %g", -L);
}

void mt_tourner_droite(machine_trace_t m, double a) {
    commande(m, "tourner %g", -a);
}

void mt_tourner_gauche(machine_trace_t m, double a) {
    commande(m, "tourner %g", a);
}

void mt_cercle(machine_trace_t m, double r) {
    commande(m, "cercle %g", r);
}

void mt_ovale(machine_trace_t m, double rx, double ry) {
    commande(m, "ovale %g %g", rx, ry);
}

void mt_image(machine_trace_t m, char *name, double w, double h) {
    commande(m, "image %s %g %g", name, w, h);
}
