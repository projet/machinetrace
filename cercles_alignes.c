#include "machine_dessin.h"

int main() {
    machine_trace_t m;

    m = mt_creer(400, 400);
    int r = 3, n = 4;

    // Attend automatiquement après chaque commande
    // Permet de voir le dessin se construire
    mt_attente_automatique(m, 100);

    mt_baisser(m);
    mt_ovale(m, 150, 75);
    mt_lever(m);
    mt_placer(m, -100, 0);
    for (int i = 0; i <= n; i++) {
        mt_baisser(m);
        mt_cercle(m, r);
        mt_lever(m);
        mt_avancer(m, 4 * r);
        r *= 2;
    }
    mt_fermer(m);
}
