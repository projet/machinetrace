#ifndef __MACHINE_TRACE_H__
#define __MACHINE_TRACE_H__

#define MT_NOIR 0
#define MT_BLANC 1
#define MT_JAUNE 2
#define MT_ROUGE 3
#define MT_VERT 4
#define MT_BLEU 5

struct machine_trace_data;

typedef struct machine_trace_data *machine_trace_t;

machine_trace_t mt_creer(int l, int h);
void mt_fermer(machine_trace_t m);

void mt_attendre(machine_trace_t m, int ms);
void mt_attente_automatique(machine_trace_t m, int ms);
void mt_avancer(machine_trace_t m, double L);
void mt_baisser(machine_trace_t m);
void mt_change_couleur(machine_trace_t m, int couleur);
void mt_effacer_tout(machine_trace_t m);
void mt_lever(machine_trace_t m);
void mt_masquer_pointeur(machine_trace_t m);
void mt_montrer_pointeur(machine_trace_t m);
void mt_orienter(machine_trace_t m, double a);
void mt_placer(machine_trace_t m, double x, double y);
void mt_rafraichir(machine_trace_t m);
void mt_rafraichissement_automatique(machine_trace_t m, int r);
void mt_reculer(machine_trace_t m, double L);
void mt_tourner_droite(machine_trace_t m, double a);
void mt_tourner_gauche(machine_trace_t m, double a);

#endif
